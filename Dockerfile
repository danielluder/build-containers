FROM ubuntu:latest

RUN apt-get update \
    && apt-get install -y cmake \
    && apt-get install -y build-essential \
    && rm -rf /var/lib/apt/lists/*